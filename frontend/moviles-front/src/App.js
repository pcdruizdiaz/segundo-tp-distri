import React from 'react'
import Banner from "./components/Banner" 
import FormMovil from "./components/FormMovil"
import FormUbicacion from "./components/FormUbicacion"
import FormBusqueda from "./components/FormBusqueda"

import Home from "./components/Home"
import { BrowserRouter as Router, Route, Switch } from "react-router-dom"

function App() {
  return (
    <div className="page-container">
      <div className="content-wrap">
        <Router>
          <Banner />
          <Switch>
            <Route path="/home" exact component={Home} />
            <Route path="/movil" exact component={FormMovil} />
            <Route path="/ubicacion" exact component={FormUbicacion} />
            <Route path="/busqueda" exact component={FormBusqueda} />
            {/* <Route path="/registro" exact component={FormComponet} />
            <Route path="/proveedor/:id" exact component={ProveedoresGrid} />
            <Route path="/proveedor/perfil/:id" exact component={ProfileProveedor} />
            <Route path="/proveedor/perfil/editar/:id" exact component={EditProfile} /> */}
            <Route component={() => (
              <div>
                <h1 className="center">Error404: NO EXISTE</h1>
              </div>
            )}
            />
          </Switch>
        </Router>
      </div>
      {/* <Footer /> */}
    </div>

  )
}

export default App;
