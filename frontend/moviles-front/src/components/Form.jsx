import React from 'react';
import { Formik, FieldArray } from 'formik';
import ValidationForm from "../utils/validationForm";
import { formatoFecha, getObjeto } from "../utils/funcionesForm";
import { Container, Row, Col, Form, Button, Image } from 'react-bootstrap';
import axios from "axios"
import * as Constants from "../constants"

let containerStyle = {
    background: '#73BFCA',
    borderRadius: "20px 20px 0px 0px",
    padding: "20px",
};

class FormComponent extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            profesiones: [],
            ciudades: [],
            img: undefined,
        }
    }

    handleImgChange(e) {
        e.preventDefault();
        let file = e.target.files;
        if (file.length > 0) {
            let reader = new FileReader();
            reader.readAsDataURL(file[0]);
            reader.onload = function () {
                this.setState({ img: reader.result });
            }.bind(this);
        }
    }

    render() {
        return (
            <Formik
                validationSchema={ValidationForm}
                onSubmit={(values) => {

                    let formPost = {
                        nombre: values.nombre,
                        apellido: values.apellido,
                        ci: values.ci,
                        fecha_nacimiento: formatoFecha(values.nacimiento),
                        telefono_wsp: values.whatsapp,
                        telefono_llamada: values.tel ? values.tel : null,
                        img_perfil: this.state.img,
                        oficios: getObjeto(values.profesion, this.state.profesiones),
                        ciudades: getObjeto(values.destino, this.state.ciudades),
                    }

                    //console.log(JSON.stringify(formPost));

                    axios.post(Constants.PROVEEDORES_URL, formPost)
                        .then(function (response) {
                            console.log(response);
                        })
                        .catch(function (error) {
                            console.log(error);
                        });
                }}
                initialValues={{
                    imgPerfil: undefined,
                    nombre: "",
                    apellido: "",
                    ci: "",
                    tel: "",
                    whatsapp: "",
                    nacimiento: "",
                    profesion: [],
                    destino: [],
                }}
            >
                {({
                    handleSubmit,
                    handleChange,
                    values,
                    touched,
                    errors,
                    setFieldValue,
                }) => (
                        <Container style={containerStyle} >
                            <Form noValidate onSubmit={handleSubmit}>
                                <Row className="justify-content-center">
                                    <Col xs="auto">
                                        {this.state.img ?
                                            <Image
                                                src={this.state.img}
                                                height={125}
                                                width={125}
                                                roundedCircle
                                            /> :
                                            <Image
                                                src="https://images.vexels.com/media/users/3/137047/isolated/preview/5831a17a290077c646a48c4db78a81bb-user-profile-blue-icon-by-vexels.png"
                                                height={125}
                                                width={125}
                                                roundedCircle
                                            />}
                                    </Col>
                                </Row>
                                <Form.Row className="justify-content-center">
                                    <Form.Group as={Col} xs="auto" controlId="imgPerfil">
                                        <Form.File
                                            className="position-relative"
                                            required
                                            name="imgPerfil"
                                            label=" "
                                            accept="image/*"
                                            onChange={e => {
                                                setFieldValue("imgPerfil", e.target.files[0]);
                                                this.handleImgChange(e);
                                            }}
                                            isInvalid={touched.imgPerfil && !!errors.imgPerfil}
                                            feedback={errors.imgPerfil}
                                            id="validationFormikFotoPerfil"
                                        />
                                    </Form.Group>
                                </Form.Row>
                                <Form.Row className="justify-content-center">
                                    <Form.Group as={Col} xs={6} md={3} controlId="nombre">
                                        <Form.Label>Nombre</Form.Label>
                                        <Form.Control
                                            type="text"
                                            name="nombre"
                                            placeholder="Mateo Giulliano"
                                            value={values.nombre}
                                            onChange={handleChange}
                                            isInvalid={touched.nombre && !!errors.nombre}
                                        />
                                        <Form.Control.Feedback type="invalid">
                                            {errors.nombre}
                                        </Form.Control.Feedback>
                                    </Form.Group>
                                    <Form.Group controlId="Tipo de Automovil">
                                        <Form.Label>Seleccione Tipo de Automovil</Form.Label>
                                        <Form.Control as="select">
                                            <option>1</option>
                                            <option>2</option>
                                            <option>3</option>
                                            <option>4</option>
                                            <option>5</option>
                                        </Form.Control>
                                    </Form.Group>
                                </Form.Row>
                                <Form.Row className="justify-content-center">
                                    <Form.Group as={Col} xs={6} md={3} controlId="nacimiento">
                                        <Form.Label>Fecha de nacimiento</Form.Label>
                                        <Form.Control
                                            type="tel"
                                            placeholder="20/05/1999"
                                            name="nacimiento"
                                            value={values.nacimiento}
                                            onChange={handleChange}
                                            isInvalid={touched.nacimiento && !!errors.nacimiento}
                                        />
                                        <Form.Control.Feedback type="invalid" >
                                            {errors.nacimiento}
                                        </Form.Control.Feedback>
                                    </Form.Group>
                                    <Form.Group as={Col} xs={6} md={3} controlId="ci">
                                        <Form.Label>Cédula de identidad</Form.Label>
                                        <Form.Control
                                            type="tel"
                                            placeholder="4484570"
                                            name="ci"
                                            value={values.ci}
                                            onChange={handleChange}
                                            isInvalid={touched.ci && !!errors.ci}
                                        />
                                        <Form.Control.Feedback type="invalid" >
                                            {errors.ci}
                                        </Form.Control.Feedback>
                                    </Form.Group>
                                </Form.Row>
                                <Form.Row className="justify-content-center">
                                    <Form.Group as={Col} xs={6} md={3} controlId="tel">
                                        <Form.Label>Número de teléfono</Form.Label>
                                        <Form.Control
                                            type="tel"
                                            placeholder="0983342745"
                                            name="tel"
                                            value={values.tel}
                                            onChange={handleChange}
                                            isInvalid={touched.tel && !!errors.tel}
                                        />
                                        <Form.Control.Feedback type="invalid" >
                                            {errors.tel}
                                        </Form.Control.Feedback>
                                    </Form.Group>
                                    <Form.Group as={Col} xs={6} md={3} controlId="whatsapp">
                                        <Form.Label>Número de whatsapp</Form.Label>
                                        <Form.Control
                                            type="tel"
                                            placeholder="0983342745"
                                            name="whatsapp"
                                            value={values.whatsapp}
                                            onChange={handleChange}
                                            isInvalid={touched.whatsapp && !!errors.whatsapp}
                                        />
                                        <Form.Control.Feedback type="invalid" >
                                            {errors.whatsapp}
                                        </Form.Control.Feedback>
                                    </Form.Group>
                                </Form.Row>
                                <Row className="justify-content-center">
                                    <Col xs={6} md={3}>
                                        <p>Escoja su profesión</p>
                                        <FieldArray
                                            name="profesion"
                                            render={arrayHelpers => (
                                                <>
                                                    {this.state.profesiones.map(category => (
                                                        <Container key={category.id}>
                                                            <label>
                                                                <input
                                                                    name="profesion"
                                                                    type="checkbox"
                                                                    value={category.id}
                                                                    checked={values.profesion.includes(category.id)}
                                                                    onChange={e => {
                                                                        if (e.target.checked) arrayHelpers.push(category.id);
                                                                        else {
                                                                            const idx = values.profesion.indexOf(category.id);
                                                                            arrayHelpers.remove(idx);
                                                                        }
                                                                    }}
                                                                />
                                                                {" " + category.nombre}
                                                            </label>
                                                        </Container>
                                                    ))}
                                                </>
                                            )}
                                        />
                                        {
                                            /*Aca debe ir requerido*/
                                        }
                                    </Col>
                                    <Col xs={6} md={3}>
                                        <p>Escoja las ciudades a las que puede llegar</p>
                                        <FieldArray
                                            name="destino"
                                            render={arrayHelpers => (
                                                <>
                                                    {this.state.ciudades.map(category => (
                                                        <Container key={category.id}>
                                                            <label>
                                                                <input
                                                                    name="destino"
                                                                    type="checkbox"
                                                                    value={category.id}
                                                                    checked={values.destino.includes(category.id)}
                                                                    onChange={e => {
                                                                        if (e.target.checked) arrayHelpers.push(category.id);
                                                                        else {
                                                                            const idx = values.destino.indexOf(category.id);
                                                                            arrayHelpers.remove(idx);
                                                                        }
                                                                    }}
                                                                />
                                                                {" " + category.nombre}
                                                            </label>
                                                        </Container>
                                                    ))}
                                                </>
                                            )}
                                        />
                                        {
                                            /*Aca debe ir requerido*/
                                        }
                                    </Col>
                                </Row>
                                <Row className="justify-content-center">
                                    <Col xs="auto">
                                        <Button type="submit">Registrarse</Button>
                                    </Col>
                                </Row>
                            </Form>
                        </Container>
                    )}
            </Formik>
        );
    }

    componentDidMount() {

        axios.get(Constants.OFICIOS_URL)
            .then(response => {
                this.setState({
                    profesiones: response.data,
                })
            })

        axios.get(Constants.CIUDADES_URL)
            .then(response => {
                this.setState({
                    ciudades: response.data,
                })
            })
    }
}

export default FormComponent;