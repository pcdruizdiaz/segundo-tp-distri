import React from 'react';
import { Formik } from 'formik';
import { Container, Row, Col, Form, Button, Table } from 'react-bootstrap';
import axios from "axios"

class FormMovil extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            tipos: [],
            moviles: [],
        }
    }
    toInt(id) {
        return parseInt(id);
    }
    render() {
        const { tipos } = this.state
        const { moviles } = this.state
        return (
            <Container>
                <Formik
                    onSubmit={(values) => {
                        let formPost = {
                            idTipo: values.TipodeAutomovil
                        }
                        console.log(formPost)
                        axios.post('http://localhost:8081/movil/rest/movil', formPost)
                            .then(function (response) {
                                alert('Se creo correctamente!');
                                window.location = "/movil"
                            })
                            .catch(function (error) {
                                console.log(error)
                                alert('Error, intente de nuevo');
                            });

                    }}
                    initialValues={{
                        TipodeAutomovil: "",

                    }}
                >
                    {({
                        handleSubmit,
                        handleChange,
                        values,
                        touched,
                        errors,
                    }) => (
                            <Container>
                                <Form noValidate onSubmit={handleSubmit}>
                                    <Form.Row >
                                        <Form.Group as={Col} controlId="TipodeAutomovil">
                                            <Form.Label>Seleccione Tipo de Automovil</Form.Label>
                                            <Form.Control
                                                size="sm"
                                                as="select"
                                                name="TipodeAutomovil"
                                                value={values.TipodeAutomovil}
                                                onChange={handleChange}
                                            >
                                                <option value="" selected disabled hidden>Elija aquí</option>
                                                {
                                                    tipos.map(t =>
                                                        (
                                                            <option value={t.id}>{t.descripcion}</option>
                                                        )
                                                    )
                                                }
                                            </Form.Control>
                                        </Form.Group>
                                    </Form.Row>
                                    <Col xs="auto" style={{ textAlign: 'right' }}>
                                        <Button type="submit" variant="outline-primary">Crear movil</Button>
                                    </Col>
                                </Form>
                            </Container>
                        )}
                </Formik>
                <br />
                <hr />
                <h5>Listado de móviles registrados en el Sistema</h5>
                <Table striped bordered hover variant="dark">
                    <thead>
                        <tr>
                            <th>#</th>
                            <th>Tipo de Movil</th>
                        </tr>
                    </thead>
                    <tbody>
                        {
                            moviles.map(m =>
                                (
                                    <tr>
                                        <td>{m.id}</td>
                                        <td>{m.tipoMovil}</td>
                                    </tr>
                                )
                            )
                        }
                    </tbody>
                </Table>
            </Container>
        );
    }
    componentDidMount() {
        axios.get('http://localhost:8081/movil/rest/tipomovil')
            .then(response => {
                this.setState({
                    tipos: response.data
                })
                console.log(this.state.tipos)
            })

        axios.get('http://localhost:8081/movil/rest/movil')
            .then(res => {
                this.setState({
                    moviles: res.data
                })
            })
    }
    
    
}

export default FormMovil;