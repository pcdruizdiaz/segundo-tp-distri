import React, {Fragment} from "react"


import { Container, Col, CardGroup} from 'react-bootstrap'
import Ubicaciones from "./Ubicaciones"
import Opciones  from "./Opciones"
class Home extends React.Component {
    render() {
        return(
            <Fragment>
                <Opciones />
                <br />
                <Ubicaciones />
            </Fragment>
        )
    }

}
export default Home