import React, { Fragment } from "react"
import axios from "axios"
/* import Banner from "./Banner" */

import { Container, Col, CardGroup, Table } from 'react-bootstrap'

class Ubicaciones extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            ubicaciones: []
        }
    }
    render() {
        const { ubicaciones } = this.state
        return (
            <Fragment>
                <Container>
                    <h5>Listado de Ubicaciones registrados en el Sistema</h5>
                    <Table striped bordered hover variant="dark">
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>id Movil</th>
                                <th>Latitud</th>
                                <th>Longitud</th>
                                <th>Fecha/hora</th>
                            </tr>
                        </thead>
                        <tbody>
                            {
                                ubicaciones.map(u => 
                                        (
                                            <tr>
                                                <td>{u.id}</td>
                                                <td>{u.idMovil}</td>
                                                <td>{u.latitud}</td>
                                                <td>{u.longitud}</td>
                                                <td>{u.fechaHora}</td>
                                            </tr>
                                        )
                                    ) 
                            }
                        </tbody>
                    </Table>
                    <br />
                </Container>
            </Fragment>
        )
    }
    componentDidMount() {
        axios.get('http://localhost:8081/movil/rest/ubicacion')
        .then(response => {
                this.setState({
                    ubicaciones: response.data
                })
            })
    }
}

export default Ubicaciones