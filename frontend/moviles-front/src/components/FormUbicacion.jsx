import React from 'react';
import { Formik } from 'formik';
import { Container, Row, Col, Form, Button, Table } from 'react-bootstrap';
import { formatoFecha} from "../utils/funcionesForm";
import axios from "axios"

class FormUbicacion extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            moviles: [],
        }
    }
    toInt(id) {
        return parseInt(id);
    }
    render() {
        
        const { moviles } = this.state
        return (
            <Container>
            <Formik
                onSubmit={(values) => {
                    let formPost = {
                        idMovil: values.IdAutomovil,
                        latitud: values.Latitud,
                        longitud: values.Longitud,
                        fechaHora: values.fecha,
                    }
                    console.log(formPost)
                    axios.post('http://localhost:8081/movil/rest/ubicacion', formPost)
                        .then(function (response) {
                            alert('Se creo correctamente!');
                            window.location = "/home"
                        })
                        .catch(function (error) {
                            console.log(error)
                            alert('Error, intente de nuevo');
                        });

                }}
                initialValues={{
                    IdAutomovil: "",

                }}
            >
                {({
                    handleSubmit,
                    handleChange,
                    values,
                    touched,
                    errors,
                }) => (
                        <Container>
                            <Form noValidate onSubmit={handleSubmit}>
                                <Form.Row className="justify-content-left">
                                    <Form.Group as={Col} xs={4} md={3} controlId="Latitud">
                                        <Form.Label>Latitud</Form.Label>
                                        <Form.Control
                                            type="text"
                                            name="Latitud"
                                            placeholder="-200"
                                            value={values.Latitud}
                                            onChange={handleChange}
                                        />
                                    </Form.Group>
                                    <Form.Group as={Col} xs={4} md={3} controlId="Longitud">
                                        <Form.Label>Longitud</Form.Label>
                                        <Form.Control
                                            type="text"
                                            name="Longitud"
                                            placeholder="-100"
                                            value={values.Longitud}
                                            onChange={handleChange}
                                        />
                                    </Form.Group>
                                </Form.Row>
                                <Form.Row className="justify-content-left">
                                    <Form.Group as={Col} xs={9} md={9} controlId="IdAutomovil">
                                        <Form.Label>Seleccione un Id de Automovil</Form.Label>
                                        <Form.Control
                                            size="sm"
                                            as="select"
                                            name="IdAutomovil"
                                            value={values.IdAutomovil}
                                            onChange={handleChange}
                                        >
                                            <option value="" selected disabled hidden>Elija aquí</option>
                                            {
                                                moviles.map(m =>
                                                    (
                                                        <option value={m.id}>{m.id} ({m.tipoMovil})</option>
                                                    )
                                                )
                                            }
                                        </Form.Control>
                                    </Form.Group>
                                </Form.Row>
                                <Col xs="auto" style={{ textAlign: 'right' }}>
                                    <Button type="submit" variant="outline-primary">Agregar Registro</Button>
                                </Col>
                            </Form>
                        </Container>
                    )}
            </Formik>
            </Container>
        );
    }
    componentDidMount() {

        axios.get('http://localhost:8081/movil/rest/movil')
            .then(res => {
                this.setState({
                    moviles: res.data
                })
            })
    }


}

export default FormUbicacion;