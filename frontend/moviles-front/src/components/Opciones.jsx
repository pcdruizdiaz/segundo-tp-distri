import React, { Fragment } from "react"
import { Link } from "react-router-dom"
import { Container, Col, CardGroup, Button } from 'react-bootstrap'

class Opciones extends React.Component {
    render() {
        return (
            <Container style={{ textAlign: 'right' }}>
                <Link to={`/movil`}>
                    <Button variant="outline-dark">Móviles</Button>{' '}
                </Link>
                <Link to={`/ubicacion`}>
                    <Button variant="outline-dark">Agregar Registro de Ubicación de Móvil</Button>{' '}
                </Link>
                <Link to={`/busqueda`}>
                    <Button variant="outline-dark">Buscar por Ubicación exacta</Button>{' '}
                </Link>
            </Container>
        )
    }
}

export default Opciones