import React from 'react';
import { Formik } from 'formik';
import { Container, Row, Col, Form, Button, Table } from 'react-bootstrap';
import { formatoRango} from "../utils/funcionesForm";
import axios from "axios"

class FormBusqueda extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            busqueda: [],
        }
    }
    toInt(id) {
        return parseInt(id);
    }
    render() {
        
        const { busqueda } = this.state
        return (
            <Container>
            <Formik
                onSubmit={(values) => {
                    let form = {
                        latitud: values.Latitud,
                        longitud: values.Longitud,
                        distancia: values.Rango,
                    }
                    var uri = `http://localhost:8081/movil/rest/ubicacion/listarPorUbicacion?latitud=${form.latitud}&longitud=${form.longitud}&distancia=${form.distancia}`;
                    var res = encodeURI(uri);
                    axios.get(res)
                    .then(res => {
                        this.setState({
                            busqueda: res.data,
                        })
                        console.log(busqueda)
                    })

                }}
                initialValues={{
                    IdAutomovil: "",

                }}
            >
                {({
                    handleSubmit,
                    handleChange,
                    values,
                    touched,
                    errors,
                }) => (
                        <Container>
                            <Form noValidate onSubmit={handleSubmit}>
                                <Form.Row className="justify-content-center">
                                    <Form.Group as={Col} xs={4} md={3} controlId="Latitud">
                                        <Form.Label>Latitud</Form.Label>
                                        <Form.Control
                                            type="text"
                                            name="Latitud"
                                            placeholder="-200"
                                            value={values.Latitud}
                                            onChange={handleChange}
                                        />
                                    </Form.Group>
                                    <Form.Group as={Col} xs={4} md={3} controlId="Longitud">
                                        <Form.Label>Longitud</Form.Label>
                                        <Form.Control
                                            type="text"
                                            name="Longitud"
                                            placeholder="-100"
                                            value={values.Longitud}
                                            onChange={handleChange}
                                        />
                                    </Form.Group>
                                    
                                    <Form.Group as={Col} xs={4} md={3} controlId="Rango">
                                        <Form.Label>Rango (en metros)</Form.Label>
                                        <Form.Control
                                            type="text"
                                            placeholder="50"
                                            title="En metros"
                                            name="Rango"
                                            value={values.Rango}
                                            onChange={handleChange}
                                        />
                                    </Form.Group>
                                </Form.Row>
                                <Col xs="auto" style={{ textAlign: 'right' }}>
                                    <Button type="submit" variant="outline-primary">Buscar</Button>
                                </Col>
                            </Form>
                        </Container>
                    )}
            </Formik>
            <br />
            {
                this.state.busqueda ?
                <Table striped bordered hover variant="dark">
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>id Movil</th>
                                <th>Latitud</th>
                                <th>Longitud</th>
                                <th>Distancia (en kilometros)</th>
                                <th>Fecha/Hora</th>
                            </tr>
                        </thead>
                        <tbody>
                            {
                                busqueda.map(u => 
                                        (
                                            <tr>
                                                <td>{u.id}</td>
                                                <td>{u.idMovil}</td>
                                                <td>{u.latitud}</td>
                                                <td>{u.longitud}</td>
                                                <td>{u.distanciaTotal}</td>
                                                <td>{u.fechaHora}</td>
                                            </tr>
                                        )
                                    ) 
                            }
                        </tbody>
                    </Table>
                    : 'null'
            }
            
            </Container>
        );
    }

}

export default FormBusqueda;