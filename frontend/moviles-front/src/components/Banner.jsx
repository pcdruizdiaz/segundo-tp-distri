import React from 'react';
import {Jumbotron, Container, Button} from 'react-bootstrap'
import { LinkContainer} from 'react-router-bootstrap'
const Banner = () => (
    <Jumbotron className="banner-principal" fluid>
        <Container>
            <h1 style={{textAlign:"center"}}>
                Móviles - Sistemas Distribuidos</h1>
        </Container>
    </Jumbotron>
)


export default Banner;