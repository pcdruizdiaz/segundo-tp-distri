export let formatoFecha = (fecha_formato) => {
    let fecha = fecha_formato;
    let vect_fecha = fecha.split("/");
    let fecha_nacimiento = vect_fecha[2] + "-" + vect_fecha[1] + "-" + vect_fecha[0];
    return fecha_nacimiento;
}
export let decodificadorFecha = (fecha_formato) => {
    let fecha = fecha_formato;
    let vect_fecha = fecha.split("-");
    let fecha_nacimiento = vect_fecha[2] + "/" + vect_fecha[1] + "/" + vect_fecha[0];
    return fecha_nacimiento;
}
let getObjetoAux = (identificador, objetos) => {
    let lista = []
    for (let index = 0; index < identificador.length; index++) {
        for (let index2 = 0; index2 < objetos.length; index2++) {
            if (identificador[index] === objetos[index2].id) {
                lista.push(objetos[index2])
                break;
            }
        }
    }
    return lista;
}
export let getObjeto = (identificador, objetos) => {
    let lista = getObjetoAux(identificador, objetos);
    for (let index = 0; index < lista.length; index++) {
        delete lista[index]['imagen'];
    }
    return lista;
}
export let decodificadorOBJ = (objeto) => {
    let list_id = [];
    for (let index = 0; index < objeto.length; index++) {
        list_id.push(objeto[index].id);
    }
    return list_id;
}