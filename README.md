# Segundo-TP-Distri

## Integrantes:
- Pedro Fabian Ramos Genes.
- Bruno Santiago Ruiz Diaz Sosa.

## Instalación y Ejecución.

**Requerimientos Utilizados**
* JavaEE, JDK 1.8.
* Servidor de Aplicaciones Wildfly 21.0.0.final (Desplegado en el Puerto 8081).
* IDE Netbeans 8.2.
* PostMan.
* Motor de base de datos Postgresql.
* Gestor de Base de Datos Dbeaver Community.
* React Js para Frontend.

**Descargar el proyecto**

Via SSH:
    
   git clone git@gitlab.com:pcdruizdiaz/segundo-tp-distri.git
    
Via HTTPS:
    
   git clone https://gitlab.com/pcdruizdiaz/segundo-tp-distri.git

**Base de datos**
 * Crear una base de datos con nombre sd.
 * Configurar los datos de configuración y acceso en la clase "Bd.java".

```sql
-- public.tipo_movil definition

-- Drop table

-- DROP TABLE public.tipo_movil;

CREATE TABLE public.tipo_movil (
	id bigserial NOT NULL,
	descripcion varchar NULL,
	CONSTRAINT id_tipo_movil_pk PRIMARY KEY (id)
);


-- public.movil definition

-- Drop table

-- DROP TABLE public.movil;

CREATE TABLE public.movil (
	id bigserial NOT NULL,
	id_tipo int8 NULL,
	CONSTRAINT id_movil_pk PRIMARY KEY (id),
	CONSTRAINT id_tipo_tipo_movil_fk FOREIGN KEY (id_tipo) REFERENCES tipo_movil(id)
);


-- public.ubicacion definition

-- Drop table

-- DROP TABLE public.ubicacion;

CREATE TABLE public.ubicacion (
	id bigserial NOT NULL,
	id_movil int8 NULL,
	latitud varchar NULL,
	longitud varchar NULL,
	fecha_hora varchar NULL,
	CONSTRAINT id_ubicacion_pk PRIMARY KEY (id),
	CONSTRAINT id_movil_movil_fk FOREIGN KEY (id_movil) REFERENCES movil(id)
);
```
**Desplegar en Servidor**
 * Desde el IDE Netbeans, configurar el servidor de aplicaciones Wildfly21.
 * Deploy del proyecto "moviles" en el servidor Wildfly.

**Correr Frontend**
 * Desde una terminal NPM, ejecutar dentro de la carpeta "moviles-front":
 ```node
npm install
npm start
```
 * Acceder a la url "http://localhost:3000/home" para acceder a las opciones de Prueba y la lista de Registro de Ubicaciones.

## Pruebas de ejecución
* Acceder a la url "http://localhost:3000/movil" para ver la lista de Moviles y formulario de creación. 
	* Método GET para listar móviles 'http://localhost:8081/movil/rest/movil'
	* Método GET para listar Tipos de Móviles 'http://localhost:8081/movil/rest/tipomovil'
	* Método POST para crear móvil 'http://localhost:8081/movil/rest/movil'
		* Ingresando los campos requeridos: Id Tipo de Movil (idTipo)

* Acceder a la url "http://localhost:3000/ubicacion" para ver el formulario de creación de Registro de Ubicaciones.
	* Método GET para listar móviles 'http://localhost:8081/movil/rest/movil' y disponibilizarlos para la creación de Registros.
	* Método POST para crear registros de ubicaciones 'http://localhost:8081/movil/rest/ubicacion' 
		* Ingresando los campos requeridos: Id Movil (idMovil), Latitud (latitud), Longitud (longitud)

* Acceder a la url "http://localhost:3000/busqueda" para buscar móviles cercanos a una ubicación exacta en una rango de metros.
	* Método GET para listar móviles `http://localhost:8081/movil/rest/ubicacion/listarPorUbicacion?latitud=${form.latitud}&longitud=${form.longitud}&distancia=${form.distancia}`
		* Los valores de Latitud, Longitud y distancia son reemplazados por los insertados por el usuario en el form.

* Imagenes de ejemplo de utilización del Frontend https://gitlab.com/pcdruizdiaz/segundo-tp-distri/imagenes_de_prueba