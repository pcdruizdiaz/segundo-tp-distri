/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.una.pol.personas.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.inject.Inject;
import py.una.pol.personas.model.Mapa;
import py.una.pol.personas.model.Movil;

/**
 *
 * @author Pedro Ramos
 */
public class MovilDAO {

    @Inject
    private Logger LOGGER;

    public List<Mapa> seleccionar() {
        String query = "select\n"
                + "	m.*, \n"
                + "	tm.descripcion as tipo_movil\n"
                + "from\n"
                + "	movil m\n"
                + "join tipo_movil tm on\n"
                + "	tm.id = m.id_tipo ;";
        Connection connect = null;
        List<Mapa> lista = new ArrayList<>();

        try {
            connect = Bd.connect();
            ResultSet rs = connect.createStatement().executeQuery(query);

            ResultSetMetaData md = rs.getMetaData();
            int columnCount = md.getColumnCount();

            while (rs.next()) {
                Mapa m = new Mapa();
                for (int i = 1; i <= columnCount; i++) {
                    m.put(md.getColumnName(i), rs.getObject(i));
                }
                lista.add(m);
            }
        } catch (SQLException ex) {
            LOGGER.log(Level.SEVERE, "Error en la seleccion: {}", ex.getMessage());
        } finally {
            try {
                if (connect != null) {
                    connect.close();
                }
            } catch (SQLException ef) {
                LOGGER.log(Level.SEVERE, "No se pudo cerrar la conexion a BD: {0}", ef.getMessage());
            }
        }
        return lista;
    }

    public long insertar(Movil m) throws SQLException {

        String SQL = "INSERT INTO movil(id_tipo) "
                + "VALUES(?)";

        long id = 0;
        Connection conn = null;

        try {
            conn = Bd.connect();
            PreparedStatement pstmt = conn.prepareStatement(SQL, Statement.RETURN_GENERATED_KEYS);
            pstmt.setLong(1, m.getIdTipo());

            int affectedRows = pstmt.executeUpdate();
            // check the affected rows 
            if (affectedRows > 0) {
                // get the ID back
                try (ResultSet rs = pstmt.getGeneratedKeys()) {
                    if (rs.next()) {
                        id = rs.getLong(1);
                    }
                } catch (SQLException ex) {
                    throw ex;
                }
            }
        } catch (SQLException ex) {
            throw ex;
        } finally {
            try {
                if (conn != null) {
                    conn.close();
                }
            } catch (SQLException ef) {
                LOGGER.log(Level.SEVERE, "No se pudo cerrar la conexion a BD: {0}", ef.getMessage());
            }
        }

        return id;

    }

}
