/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.una.pol.personas.model;

import java.io.Serializable;

/**
 *
 * @author Bruno Ruiz Diaz
 */
public class Movil implements Serializable{

    private Long id;
    private Long idTipo;

    public Movil(Long id, Long idTipo) {
        this.id = id;
        this.idTipo = idTipo;
    }

    public Movil() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getIdTipo() {
        return idTipo;
    }

    public void setIdTipo(Long idTipo) {
        this.idTipo = idTipo;
    }

    @Override
    public String toString() {
        return "Movil{" + "id=" + id + ", idTipo=" + idTipo + '}';
    }
}
