/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.una.pol.personas.model;

import java.io.Serializable;
import java.util.Date;

/**
 *
 * @author Bruno Ruiz Diaz
 */
public class Ubicacion implements Serializable {
    private Long id;
    private Long idMovil;
    private String latitud;
    private String longitud;
    private String fechaHora;

    public Ubicacion(Long id, Long idMovil, String latitud, String longitud, String fechaHora) {
        this.id = id;
        this.idMovil = idMovil;
        this.latitud = latitud;
        this.longitud = longitud;
        this.fechaHora = fechaHora;
    }

    public Ubicacion() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getIdMovil() {
        return idMovil;
    }

    public void setIdMovil(Long idMovil) {
        this.idMovil = idMovil;
    }

    public String getLatitud() {
        return latitud;
    }

    public void setLatitud(String latitud) {
        this.latitud = latitud;
    }

    public String getLongitud() {
        return longitud;
    }

    public void setLongitud(String longitud) {
        this.longitud = longitud;
    }

    public String getFechaHora() {
        return fechaHora;
    }

    public void setFechaHora(String fechaHora) {
        this.fechaHora = fechaHora;
    }
    
    
}
