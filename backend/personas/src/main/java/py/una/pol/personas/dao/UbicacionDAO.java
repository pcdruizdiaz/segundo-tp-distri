/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.una.pol.personas.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.inject.Inject;
import py.una.pol.personas.model.Mapa;
import py.una.pol.personas.model.Ubicacion;
import py.una.pol.personas.model.UbicacionDTO;

/**
 *
 * @author Pedro Ramos
 */
public class UbicacionDAO {

    @Inject
    private Logger LOGGER;

    public List<Ubicacion> seleccionar() {
        String query = "select * from ubicacion";
        Connection connect = null;
        List<Ubicacion> lista = new ArrayList<>();

        try {
            connect = Bd.connect();
            ResultSet rs = connect.createStatement().executeQuery(query);

            while (rs.next()) {
                Ubicacion u = new Ubicacion();
                u.setId(rs.getLong(1));
                u.setIdMovil(rs.getLong(2));
                u.setLatitud(rs.getString(3));
                u.setLongitud(rs.getString(4));
                u.setFechaHora(rs.getString(5));
//                Timestamp timestamp = rs.getTimestamp(5);
//                ZonedDateTime utcDateTime = ZonedDateTime.ofInstant(timestamp.toInstant(), ZoneId.of("UTF-04"));
//                LocalDateTime localDateTime = utcDateTime.withZoneSameInstant(ZoneId.systemDefault()).toLocalDateTime();
//                
//                u.setFechaHora(Date.from(localDateTime.atZone(ZoneId.systemDefault()).toInstant()));
                lista.add(u);
            }

        } catch (SQLException ex) {
            LOGGER.log(Level.SEVERE, "Error en la seleccion: {}", ex.getMessage());
        } finally {
            try {
                if (connect != null) {
                    connect.close();
                }
            } catch (SQLException ef) {
                LOGGER.log(Level.SEVERE, "No se pudo cerrar la conexion a BD: {0}", ef.getMessage());
            }
        }
        return lista;
    }

    public long insertar(UbicacionDTO u) throws SQLException, ParseException {

        String SQL = "INSERT INTO ubicacion(id_movil, latitud, longitud, fecha_hora) "
                + "VALUES(?,?,?,?)";

        long id = 0;
        
        Connection conn = null;

        try {
            conn = Bd.connect();
            PreparedStatement pstmt = conn.prepareStatement(SQL, Statement.RETURN_GENERATED_KEYS);
            pstmt.setLong(1, u.getIdMovil());
            pstmt.setString(2, u.getLatitud());
            pstmt.setString(3, u.getLongitud());
            java.util.Date date = new java.util.Date(new java.util.Date().getTime());
            pstmt.setString(4, new SimpleDateFormat("dd/MM/yyyy HH:mm:ss").format(date));
            int affectedRows = pstmt.executeUpdate();
            // check the affected rows 
            if (affectedRows > 0) {
                // get the ID back
                try (ResultSet rs = pstmt.getGeneratedKeys()) {
                    if (rs.next()) {
                        id = rs.getLong(1);
                    }
                } catch (SQLException ex) {
                    throw ex;
                }
            }
        } catch (SQLException ex) {
            throw ex;
        } finally {
            try {
                if (conn != null) {
                    conn.close();
                }
            } catch (SQLException ef) {
                LOGGER.log(Level.SEVERE, "No se pudo cerrar la conexion a BD: {0}", ef.getMessage());
            }
        }

        return id;

    }

    public List<Ubicacion> seleccionarPorId(Long id) {

        String query = "select * from ubicacion where id_movil=?";
        Connection connect = null;
        List<Ubicacion> lista = new ArrayList<>();

        try {
            connect = Bd.connect();
            PreparedStatement prepareStatement = connect.prepareStatement(query);
            prepareStatement.setLong(1, id);
            ResultSet rs = prepareStatement.executeQuery();

            while (rs.next()) {
                Ubicacion u = new Ubicacion();
                u.setId(rs.getLong(1));
                u.setIdMovil(rs.getLong(2));
                u.setLatitud(rs.getString(3));
                u.setLongitud(rs.getString(4));
                u.setFechaHora(rs.getString(5));

                lista.add(u);
            }

        } catch (SQLException ex) {
            LOGGER.log(Level.SEVERE, "Error en la seleccion: {}", ex.getMessage());
        } finally {
            try {
                if (connect != null) {
                    connect.close();
                }
            } catch (SQLException ef) {
                LOGGER.log(Level.SEVERE, "No se pudo cerrar la conexion a BD: {0}", ef.getMessage());
            }
        }
        return lista;

    }

    public List<Mapa> seleccionarMovilPorDistancia(String latitud, String longitud, String distancia) {
        Double distanciaD = Double.valueOf(distancia) / 1000;
        String query = "select\n"
                + "	acos(sin(radians(cast(:lat as float8)))* sin(radians(cast(latitud as float8))) + cos(radians(cast(:lat as float8)))* cos(radians(cast(latitud as float8)))* cos(radians(cast(longitud as float8)) - radians(cast(:lon as float8)))) * :R as distancia_total,\n"
                + "	u.id as id_ubicacion,\n"
                + "	tm.id as id_tipo_movil,\n"
                + "	*\n"
                + "from\n"
                + "	movil m\n"
                + "join ubicacion u on\n"
                + "	u.id_movil = m.id\n"
                + "join tipo_movil tm on\n"
                + "	tm.id = m.id_tipo\n"
                + "where\n"
                + "	acos(sin(radians(cast(:lat as float8)))* sin(radians(cast(latitud as float8))) + cos(radians(cast(:lat as float8)))* cos(radians(cast(latitud as float8)))* cos(radians(cast(longitud as float8)) - radians(cast(:lon as float8)))) * :R <= :distancia";
        Connection connect = null;
        List<Mapa> lista = new ArrayList<>();

        try {
            connect = Bd.connect();
            query = query.replaceAll(":lat", latitud).replaceAll(":lon", longitud).replaceAll(":distancia", String.valueOf(distanciaD)).replaceAll(":R", "6371");
            ResultSet rs = connect.createStatement().executeQuery(query);
            ResultSetMetaData md = rs.getMetaData();
            int columnCount = md.getColumnCount();

            while (rs.next()) {
                Mapa m = new Mapa();
                for (int i = 1; i <= columnCount; i++) {
                    m.put(md.getColumnName(i), rs.getObject(i));
                }
                lista.add(m);
            }

        } catch (SQLException ex) {
            LOGGER.log(Level.SEVERE, "Error en la seleccion: {}", ex.getMessage());
        } finally {
            try {
                if (connect != null) {
                    connect.close();
                }
            } catch (SQLException ef) {
                LOGGER.log(Level.SEVERE, "No se pudo cerrar la conexion a BD: {0}", ef.getMessage());
            }
        }
        return lista;

    }

}
