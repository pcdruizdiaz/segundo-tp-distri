/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.una.pol.personas.model;

import java.util.HashMap;
import java.util.Map;

/**
 *
 * @author Pedro Genes
 */
public class Mapa<K, V> extends HashMap<String, V> implements Map<String, V> {

    @Override
    public V put(String key, V value) {
        return super.put(this.toCamelCase(key), value);
    }

    public String toCamelCase(String s) {
        StringBuilder sb = new StringBuilder(s.toLowerCase());
        for (int i = 0; i < sb.length(); i++) {
            if (sb.charAt(i) == '_') {
                sb.deleteCharAt(i);
                sb.replace(i, i + 1, String.valueOf(Character.toUpperCase(sb.charAt(i))));
            }
        }
        return sb.toString();

    }

}
