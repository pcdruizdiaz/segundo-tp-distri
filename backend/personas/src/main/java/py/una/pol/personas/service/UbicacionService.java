/*
 * JBoss, Home of Professional Open Source
 * Copyright 2013, Red Hat, Inc. and/or its affiliates, and individual
 * contributors by the @authors tag. See the copyright.txt in the
 * distribution for a full listing of individual contributors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package py.una.pol.personas.service;


import javax.ejb.Stateless;
import javax.inject.Inject;

import py.una.pol.personas.dao.UbicacionDAO;
import py.una.pol.personas.model.Ubicacion;

import java.util.List;
import java.util.logging.Logger;
import py.una.pol.personas.model.Mapa;
import py.una.pol.personas.model.UbicacionDTO;

// The @Stateless annotation eliminates the need for manual transaction demarcation
@Stateless
public class UbicacionService {

    @Inject
    private Logger log;

    @Inject
    private UbicacionDAO dao;

    public void crear(UbicacionDTO u) throws Exception {
        log.info("Creando Ubicacion....");
        try {
        	dao.insertar(u);
        }catch(Exception e) {
        	log.severe("ERROR al crear ubicacion: " + e.getLocalizedMessage() );
        	throw e;
        }
        log.info("Ubicacion creada con éxito");
    }
    
    public List<Ubicacion> seleccionar() {
    	return dao.seleccionar();
    }
//    
//    public Persona seleccionarPorCedula(long cedula) {
//    	return dao.seleccionarPorCedula(cedula);
//    }
//    
//    public long borrar(long cedula) throws Exception {
//    	return dao.borrar(cedula);
//    }

    public List<Ubicacion> seleccionarPorId(Long id) {
        return dao.seleccionarPorId(id);
    }
    
    public List<Mapa> seleccionarMovilPorDistancia(String latitud, String longitud, String distancia) {
        return dao.seleccionarMovilPorDistancia(latitud, longitud, distancia);
    }
    
}
