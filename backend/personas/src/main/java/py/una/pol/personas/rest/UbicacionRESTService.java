/*
 * JBoss, Home of Professional Open Source
 * Copyright 2013, Red Hat, Inc. and/or its affiliates, and individual
 * contributors by the @authors tag. See the copyright.txt in the
 * distribution for a full listing of individual contributors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package py.una.pol.personas.rest;

import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Logger;

import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import py.una.pol.personas.model.Mapa;

import py.una.pol.personas.model.Ubicacion;
import py.una.pol.personas.model.UbicacionDTO;
import py.una.pol.personas.service.UbicacionService;

/**
 * JAX-RS Example
 * <p/>
 * Esta clase produce un servicio RESTful para leer y escribir contenido de personas
 */
@Path("/ubicacion")
@RequestScoped
public class UbicacionRESTService {

    @Inject
    private Logger log;

    @Inject
    UbicacionService ubicacionService;

    @GET
    @Produces({ MediaType.APPLICATION_JSON})
    
    public List<Ubicacion> listar() {
        return ubicacionService.seleccionar();
    }
    
    @GET
    @Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
    @Path("/listarPorId")
    public List<Ubicacion> listarPorId(@QueryParam("id") Long id) {
        return ubicacionService.seleccionarPorId(id);
    }
    
    
    @GET
    @Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
    @Path("/listarPorUbicacion")
    public List<Mapa> seleccionarMovilPorDistancia(@QueryParam("latitud") String latitud, @QueryParam("longitud") String longitud, @QueryParam("distancia") String distancia) {
        return ubicacionService.seleccionarMovilPorDistancia(latitud, longitud, distancia);
    }
    //@GET
    //@Path("/id")
    //@Produces(MediaType.APPLICATION_JSON)
    //public List<Ubicacion> obtenerPorId(@PathParam("id") long id) {
        //List <Ubicacion> lista = ubicacionService.seleccionarPorId(id);
        //if (u == null) {
        //	log.info("obtenerPorId " + id + " no encontrado.");
         //   throw new WebApplicationException(Response.Status.NOT_FOUND);
        //}
       // log.info("obtenerPorId " + id + " encontrada: " + u.getId());
       // return p;
    //}
//
//    @GET
//    @Path("/{cedula:[0-9][0-9]*}")
//    @Produces(MediaType.APPLICATION_JSON)
//    public Persona obtenerPorId(@PathParam("cedula") long cedula) {
//        Persona p = personaService.seleccionarPorCedula(cedula);
//        if (p == null) {
//        	log.info("obtenerPorId " + cedula + " no encontrado.");
//            throw new WebApplicationException(Response.Status.NOT_FOUND);
//        }
//        log.info("obtenerPorId " + cedula + " encontrada: " + p.getNombre());
//        return p;
//    }
//
//    @GET
//    @Path("/cedula")
//    @Produces(MediaType.APPLICATION_JSON)
//    public Persona obtenerPorIdQuery(@QueryParam("cedula") long cedula) {
//        Persona p = personaService.seleccionarPorCedula(cedula);
//        if (p == null) {
//        	log.info("obtenerPorId " + cedula + " no encontrado.");
//            throw new WebApplicationException(Response.Status.NOT_FOUND);
//        }
//        log.info("obtenerPorId " + cedula + " encontrada: " + p.getNombre());
//        return p;
//    }
//
//    
//    
//    /**
//     * Creates a new member from the values provided. Performs validation, and will return a JAX-RS response with either 200 ok,
//     * or with a map of fields, and related errors.
//     */
    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response crear(UbicacionDTO u) {

        Response.ResponseBuilder builder = null;
      
        try {
            ubicacionService.crear(u);
            // Create an "ok" response
            
            //builder = Response.ok();
            Map<String, String> responseObj = new HashMap<>();
            responseObj.put("estado", "OK");
            builder = Response.status(201).entity(responseObj);
            
        } catch (SQLException e) {
            // Handle the unique constrain violation
            Map<String, String> responseObj = new HashMap<>();
            responseObj.put("bd-error", e.getLocalizedMessage());
            builder = Response.status(Response.Status.CONFLICT).entity(responseObj);
        } catch (Exception e) {
            // Handle generic exceptions
            Map<String, String> responseObj = new HashMap<>();
            responseObj.put("error", e.getMessage());
            builder = Response.status(Response.Status.BAD_REQUEST).entity(responseObj);
        }

        return builder.build();
    }
//
//   @DELETE
//   @Path("/{cedula}")
//   public Response borrar(@PathParam("cedula") long cedula)
//   {      
//	   Response.ResponseBuilder builder = null;
//	   try {
//		   
//		   if(personaService.seleccionarPorCedula(cedula) == null) {
//			   
//			   builder = Response.status(Response.Status.NOT_ACCEPTABLE).entity("Persona no existe.");
//		   }else {
//			   personaService.borrar(cedula);
//			   builder = Response.status(202).entity("Persona borrada exitosamente.");			   
//		   }
//		   
//
//		   
//	   } catch (SQLException e) {
//           // Handle the unique constrain violation
//           Map<String, String> responseObj = new HashMap<>();
//           responseObj.put("bd-error", e.getLocalizedMessage());
//           builder = Response.status(Response.Status.CONFLICT).entity(responseObj);
//       } catch (Exception e) {
//           // Handle generic exceptions
//           Map<String, String> responseObj = new HashMap<>();
//           responseObj.put("error", e.getMessage());
//           builder = Response.status(Response.Status.BAD_REQUEST).entity(responseObj);
//       }
//       return builder.build();
//   }
   
}
