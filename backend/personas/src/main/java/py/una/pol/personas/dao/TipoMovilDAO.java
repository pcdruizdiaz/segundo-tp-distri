/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.una.pol.personas.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.inject.Inject;
import py.una.pol.personas.model.TipoMovil;

/**
 *
 * @author Pedro Ramos
 */
public class TipoMovilDAO {

    @Inject
    private Logger LOGGER;

    public List<TipoMovil> seleccionar() {
        String query = "select * from tipo_movil";
        Connection connect = null;
        List<TipoMovil> lista = new ArrayList<>();

        try {
            connect = Bd.connect();
            ResultSet rs = connect.createStatement().executeQuery(query);

            while (rs.next()) {
                TipoMovil tm = new TipoMovil();
                tm.setId(rs.getLong(1));
                tm.setDescripcion(rs.getString(2));

                lista.add(tm);
            }

        } catch (SQLException ex) {
            LOGGER.log(Level.SEVERE, "Error en la seleccion: {}", ex.getMessage());
        } finally {
            try {
                if (connect != null) {
                    connect.close();
                }
            } catch (SQLException ef) {
                LOGGER.log(Level.SEVERE, "No se pudo cerrar la conexion a BD: {0}", ef.getMessage());
            }
        }
        return lista;
    }

    public long insertar(TipoMovil tm) throws SQLException {

        String SQL = "INSERT INTO tipo_movil(descripcion) "
                + "VALUES(?)";

        long id = 0;
        Connection conn = null;

        try {
            conn = Bd.connect();
            PreparedStatement pstmt = conn.prepareStatement(SQL, Statement.RETURN_GENERATED_KEYS);
            pstmt.setString(1, tm.getDescripcion());

            int affectedRows = pstmt.executeUpdate();
            // check the affected rows 
            if (affectedRows > 0) {
                // get the ID back
                try (ResultSet rs = pstmt.getGeneratedKeys()) {
                    if (rs.next()) {
                        id = rs.getLong(1);
                    }
                } catch (SQLException ex) {
                    throw ex;
                }
            }
        } catch (SQLException ex) {
            throw ex;
        } finally {
            try {
                if (conn != null) {
                    conn.close();
                }
            } catch (SQLException ef) {
                LOGGER.log(Level.SEVERE, "No se pudo cerrar la conexion a BD: {0}", ef.getMessage());
            }
        }

        return id;

    }

}
