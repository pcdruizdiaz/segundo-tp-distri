/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.una.pol.personas.model;

import java.io.Serializable;

/**
 *
 * @author Bruno Ruiz Diaz
 */
public class TipoMovil implements Serializable {
    private Long id;
    private String descripcion;

    public TipoMovil(Long id, String descripcion) {
        this.id = id;
        this.descripcion = descripcion;
    }

    public TipoMovil() {
    }

    
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    @Override
    public String toString() {
        return "TipoMovil{" + "id=" + id + ", descripcion=" + descripcion + '}';
    }
    
    
}
